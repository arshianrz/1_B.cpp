#include "shape.h"
#include <iostream>

using namespace std;


shape::shape()
{
    //Also could use run() function in our constructor.
    setN(1);

}

void shape::setN(int N)
{
    n_=N;
}

void shape::setRow(int R)
{
    row_=R;
}

int shape::getRow()
{
    return row_;
}

void shape::display()
{
    for(int i=1; i<=row_;i++)
    {
        for(int j=1; j<=i;j++)
        {
            cout<<n_<<"\t";
            n_++;
        }
        cout<<endl;
    }
}

void shape::run()
{
    int a;
    cout<<"Enter Number of Rows : \n";
    cin>>a;

    setRow(a);
    getRow();
    display();
}


