#ifndef SHAPE_H
#define SHAPE_H


class shape
{
private:
    int n_,row_;
public:
    shape();
    void setN(int);
    void setRow(int);
    int getRow();
    void display();
    void run();
};

#endif // SHAPE_H
